from random import randint

user_name = input('Hi, what is your name?\n>> ')
guess_number = 1
month = {
    1: "January",
    2: "February",
    3: "March",
    4: "April",
    5: "May",
    6: "June",
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December",
}

for guess in range(5):
    birth_month = randint(1, 12)
    birth_year = randint(1924, 2004)
    comp_guess = input(
        f'Guess Number {guess_number} : {user_name} were you born in {month[birth_month]} / {birth_year} ?\n Yes or No? ')
    if comp_guess.lower() == "yes":
        print('I knew it!')
        break
    elif comp_guess.lower() == "no" and guess_number <= 4:
        guess_number += 1
        print('Drat! Lemme try again!')
    else:
        print('I have other things to do. Goodbye.')
